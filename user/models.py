from django.contrib.auth.models import (
    AbstractUser,
    PermissionsMixin,
    UserManager,
)
from django.db import models
from django.utils.translation import gettext_lazy as _


class CustomUserManager(UserManager):
    """Custom User Manager"""

    def create_user(self, email, password, **extra_fields):
        """
        Create and save a user with the given email, and password.
        """

        if email is None:
            raise TypeError('Users must have an email address.')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user


class User(AbstractUser, PermissionsMixin):
    """Custom User model"""

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ('password',)

    username = models.CharField(
        blank=True,
        max_length=150,
    )
    email = models.EmailField(_("email address"), db_index=True, unique=True)

    objects = CustomUserManager()
