from rest_framework import generics, mixins, viewsets

from bm_service.models import Bookmark, BookmarkCollection, Collection
from bm_service.serializers import (
    BookmarkAddCollectionSerializer,
    BookmarkSerializer,
    CollectionSerializer,
)
from bm_service.services import CollectionService


class BookmarkViewSet(mixins.CreateModelMixin,
                      mixins.RetrieveModelMixin,
                      mixins.DestroyModelMixin,
                      mixins.ListModelMixin,
                      viewsets.GenericViewSet):

    queryset = Bookmark.objects.all()
    serializer_class = BookmarkSerializer

    def get_queryset(self):
        return Bookmark.objects.filter(owner=self.request.user.id)


class CollectionViewSet(mixins.CreateModelMixin,
                        mixins.RetrieveModelMixin,
                        mixins.DestroyModelMixin,
                        mixins.ListModelMixin,
                        mixins.UpdateModelMixin,
                        viewsets.GenericViewSet,
                        ):

    queryset = Collection.objects.all()
    serializer_class = CollectionSerializer
    http_method_names = ("patch", "get", "post", "delete")

    def get_queryset(self):
        return Collection.objects.filter(owner=self.request.user.id)


class BookmarkAddCollectionView(generics.CreateAPIView):
    """Controller add bookmark to collection"""

    queryset = BookmarkCollection.objects.all()
    serializer_class = BookmarkAddCollectionSerializer

    service: CollectionService = CollectionService()

    def perform_create(self, serializer: BookmarkAddCollectionSerializer):
        self.service.add_bookmark_to_collection(self.request, serializer)
