from django.contrib.auth import get_user_model
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _


User = get_user_model()


class LinkTypeChoices(models.TextChoices):
    WEBSITE = "WS", _("Website")
    BOOK = "BK", _("Book")
    ARTICLE = "AR", _("Article")
    MUSIC = "MC", _("Music")
    VIDEO = "VI", _("Video")


class BaseModel(models.Model):
    """Base model"""

    created_at = models.DateTimeField(default=timezone.now)
    updated_at = models.DateTimeField(auto_now=True)
    description = models.TextField(blank=True)

    class Meta:
        abstract = True


class Collection(BaseModel):
    """Collection model"""

    name = models.CharField(max_length=255, unique=True, blank=False)
    owner = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name='collections',
    )


class Bookmark(BaseModel):
    """Bookmarks model"""

    title = models.CharField(max_length=255)
    link = models.URLField()
    link_type = models.CharField(
        max_length=20,
        choices=LinkTypeChoices.choices,
        default=LinkTypeChoices.WEBSITE,
    )
    preview_img = models.URLField()
    owner = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name='bookmarks',
    )
    collections = models.ManyToManyField(
        Collection,
        through='BookmarkCollection',
        related_name="bookmarks",
    )

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["link", "owner"],
                name="one_bookmark_for_owner",
            )
        ]


class BookmarkCollection(models.Model):
    collection = models.ForeignKey(
        Collection,
        null=True,
        on_delete=models.SET_NULL,
    )
    bookmark = models.ForeignKey(
        Bookmark,
        on_delete=models.CASCADE,
    )

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["collection", "bookmark"],
                name="one_bookmark_in_collection",
            )
        ]
