from django.db import IntegrityError
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from bm_service.models import Bookmark, BookmarkCollection, Collection
from bm_service.services import OpenGraphService


class BookmarkSerializer(serializers.ModelSerializer):
    owner = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:
        model = Bookmark
        fields = "__all__"
        read_only_fields = (
            "id",
            "owner",
            "created_at",
            "updated_at",
            "title",
            "description",
            "link_type",
            "preview_img",
            "collections",
        )

    service: OpenGraphService = OpenGraphService()

    def create(self, validated_data):
        try:
            data = self.service.get_data(validated_data['link'])

            return Bookmark.objects.create(
                **data,
                owner=self.context['request'].user,
                link=validated_data['link'],
            )
        except IntegrityError:
            raise ValidationError(
                {"detail": "The link is already in this bookmarks"},
            )


class CollectionSerializer(serializers.ModelSerializer):
    owner = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:
        model = Collection
        fields = "__all__"
        read_only_fields = ("owner", "created_at", "updated_at")


class BookmarkAddCollectionSerializer(serializers.ModelSerializer):

    class Meta:
        model = BookmarkCollection
        fields = ("bookmark", "collection")
        read_only_fields = ("owner", "created_at", "updated_at")
