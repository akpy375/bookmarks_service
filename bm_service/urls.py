from django.urls import path
from rest_framework.routers import SimpleRouter

from bm_service.views import (
    BookmarkAddCollectionView,
    BookmarkViewSet,
    CollectionViewSet,
)

router = SimpleRouter()
router.register(r"bookmark", BookmarkViewSet, basename="bookmark")
router.register(r"collection", CollectionViewSet, basename="collection")
urlpatterns = router.urls

urlpatterns = [
    path(
        "bookmark/collection/",
        BookmarkAddCollectionView.as_view(),
        name="add_bookmark_to_collection",
    ),
] + router.urls
