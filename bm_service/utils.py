import logging


def get_logger(name):
    """Get logger"""
    logger = logging.getLogger(name)
    logger.setLevel(logging.INFO)
    handler = logging.StreamHandler()
    handler.setFormatter(
        logging.Formatter(
            "%(levelname)s:%(asctime)s - %(filename)s: %(message)s"
        )
    )
    logger.addHandler(handler)
    return logger
