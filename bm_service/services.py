import requests
from bs4 import BeautifulSoup
from django.db import IntegrityError
from django.http import JsonResponse
from opengraph_parse import parse_page
from rest_framework import status
from rest_framework.exceptions import ParseError, ValidationError

from bm_service.utils import get_logger

logger = get_logger(__file__)


class CollectionService():

    def add_bookmark_to_collection(self, request, serializer):
        if serializer.validated_data["bookmark"].owner != request.user:
            raise ValidationError({"detail": "You don't have this bookmark"})
        try:
            serializer.save()
            logger.info(
                f"Bookmark id `{serializer.validated_data['bookmark'].id}` add"
                f" to collection id ` "
                f"{serializer.validated_data['collection'].id}`"
            )
        except IntegrityError:
            raise ValidationError(
                {"detail": "The bookmark is already in this collection"},
            )


class BeautifulSoupService():

    def _parse_title(self, soup: BeautifulSoup) -> str:
        """Return tag title."""
        return (
            soup.title.string
            if soup.title and soup.title.string
            else ''
        )

    def _parse_description(self, soup: BeautifulSoup) -> str:
        """Return tag description."""
        if metatag := soup.find('meta', {'name': 'description'}):
            return metatag.get('content')
        return ""

    def _get_site_soup(self, url: str) -> BeautifulSoup:

        response = requests.get(url)
        if not response.ok:
            raise JsonResponse({"error": "Server Error (500)"},
                               status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        if not response.content:
            raise ParseError
        return BeautifulSoup(response.content, features='html.parser')


class OpenGraphService():

    bs_service: BeautifulSoupService = BeautifulSoupService()

    def get_data(self, link):
        data = self._parse_tags_data(link)
        soup = self.bs_service._get_site_soup(link)
        if data["title"] is None:
            data["title"] = self.bs_service._parse_title(soup)
        if data["description"] is None:
            data["description"] = self.bs_service._parse_description(soup)

        return data

    def _parse_tags_data(self, link) -> dict:
        parsed_data: dict = {}
        parsed_og_tags = parse_page(link,
                                    [
                                        "og:title",
                                        "og:description",
                                        "og:type",
                                        "og:image",
                                    ])

        parsed_data.setdefault("title", parsed_og_tags.get("og:title"))
        parsed_data.setdefault(
            "description", parsed_og_tags.get("og:description"),
        )

        if link_type := parsed_og_tags.get("og:type"):
            parsed_data['link_type'] = link_type
        if preview_img := parsed_og_tags.get("og:image"):
            parsed_data['preview_img'] = preview_img

        return parsed_data
