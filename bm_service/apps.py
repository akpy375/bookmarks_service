from django.apps import AppConfig


class BmServiceConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'bm_service'
